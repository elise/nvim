# NVim Config

This is my NVim config for personal use with rust development. It depends on new NVim features and you should build from source.

## Dependencies

- NVim v0.5.0+
- rust-analyzer (for Rust LSP)
- pyright (for Python LSP)
- vscode-html-languageserver (for HTML LSP)
- typescript & typescript-language-server (for TypeScript LSP)

## Installing Config

Clone this repo to ~/.config/nvim

```sh
mkdir -p ~/.config/nvim
git clone https://git.sr.ht/~ee/nvim ~/.config/nvim
```

Execute `:PackerUpdate` inside NVim

## Installing NVim

Clone [NVim](https://github.com/neovim/neovim) and enter the directory, optionally checking out the nightly tag:

```sh
git clone https://github.com/neovim/neovim.git
cd neovim
git checkout nightly # optional
```

Build and install NVim (the snippet installs to $HOME/local/nvim):

```sh
make CMAKE_BUILD_TYPE=RelWithDebInfo
make CMAKE_INSTALL_PREFIX=$HOME/local/nvim install
```

To update NVim (the snippet assumes your install is located at $HOME/local/nvim):

```sh
git checkout master
make distclean
rm -rf $HOME/local/nvim
git pull
git checkout nightly # optional
make CMAKE_BUILD_TYPE=RelWithDebInfo
make CMAKE_INSTALL_PREFIX=$HOME/local/nvim install
```

