local execute = vim.api.nvim_command
local fn = vim.fn

local install_path = '~/.local/share/nvim/site/pack/packer/start/packer.nvim'

if fn.empty(fn.glob(install_path)) > 0 then
  execute('!git clone https://github.com/wbthomason/packer.nvim '..install_path)
end

return require('packer').startup(function()
  use 'wbthomason/packer.nvim'
  -- QOL
  use 'terryma/vim-smooth-scroll' 
  use { 'kyazdani42/nvim-tree.lua', requires = 'kyazdani42/nvim-web-devicons' }
  use { 'iamcco/markdown-preview.nvim', run = 'cd app && yarn install', cmd = 'MarkdownPreview'}
  use { 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate' }
  use 'sbdchd/neoformat'
  use 'vim-scripts/dbext.vim'
  use 'karb94/neoscroll.nvim'
  -- UI
  use { 'glepnir/galaxyline.nvim', branch = 'main', requires = {'kyazdani42/nvim-web-devicons'} }
  use { 'romgrk/barbar.nvim', requires = { 'kyazdani42/nvim-web-devicons' } }
  use { 'lewis6991/gitsigns.nvim', requires = { 'nvim-lua/plenary.nvim' } }
  -- Color Themes
  use 'tjdevries/colorbuddy.vim'
  use 'marko-cerovac/material.nvim'
  -- LSP
  use 'neovim/nvim-lspconfig'
  use 'glepnir/lspsaga.nvim'
  use { 'ojroques/nvim-lspfuzzy', requires = {{'junegunn/fzf'}, {'junegunn/fzf.vim'}} }
  use 'kosayoda/nvim-lightbulb'
  use { 'hrsh7th/nvim-compe', requires = {{'hrsh7th/vim-vsnip'}, {'norcalli/snippets.nvim'}} }
  use 'nvim-lua/completion-nvim'
  use 'norcalli/nvim-colorizer.lua'
end)

