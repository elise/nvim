vim.o.completeopt = "menuone,noselect"
vim.o.termguicolors = true
vim.wo.cursorline = true
vim.wo.number = true
vim.wo.numberwidth = 4

vim.g.nvim_tree_side = "right"
vim.g.nvim_tree_auto_open = 1
vim.g.nvim_tree_auto_close = 0
vim.g.nvim_tree_indent_markers = 1
vim.g.nvim_tree_add_trailing = 1
vim.g.nvim_tree_tab_open = 1

vim.g.material_style = "palenight"

vim.api.nvim_exec(
[[
set mouse=a
syntax enable
autocmd CursorHold,CursorHoldI * lua require'nvim-lightbulb'.update_lightbulb()
]], false)
