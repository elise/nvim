require('nvim-treesitter.configs').setup {
  ensure_installed = "maintained",
  highlight = {
    enable = true,
  },
}
require('gitsigns').setup()
require('nvim-tree')

require('neoscroll')

require('lsp-config')

require('colorbuddy').colorscheme('material')
require('colorizer').setup()
