vim.api.nvim_set_keymap("i", "<Tab>", "compe#confirm('<Tab>')", {expr = true})

vim.api.nvim_set_keymap("i", "<S-Tab>", [[<cmd>lua require('lspsaga.codeaction').code_action()<CR>]], {})
vim.api.nvim_set_keymap("", "<S-Tab>", [[<cmd>lua require('lspsaga.codeaction').code_action()<CR>]], {})
vim.api.nvim_set_keymap("i", "<C-S-Tab>", [[<cmd>lua require('lspsaga.provider').lsp_finder()<CR>]], {})
vim.api.nvim_set_keymap("", "<C-S-Tab>", [[<cmd>lua require('lspsaga.provider').lsp_finder()<CR>]], {})

vim.api.nvim_set_keymap("i", "<A-down>", [[<C-o>dd<C-o>p]], {})
vim.api.nvim_set_keymap("", "<A-down>", [[ddp]], {})
vim.api.nvim_set_keymap("i", "<A-up>", [[<Up><C-o>dd<C-o>p<Up>]], {})
vim.api.nvim_set_keymap("", "<A-up>", [[<Up>ddp<Up>]], {})

vim.api.nvim_set_keymap("n", "<F1>", [[<cmd>BufferPrevious<CR>]], {})
vim.api.nvim_set_keymap("n", "<F2>", [[<cmd>BufferNext<CR>]], {})
vim.api.nvim_set_keymap("n", "<F3>", [[<cmd>BufferMovePrevious<CR>]], {})
vim.api.nvim_set_keymap("n", "<F4>", [[<cmd>BufferMoveNext<CR>]], {})
vim.api.nvim_set_keymap("n", "<F5>", [[<cmd>BufferClose<CR>]], {})
