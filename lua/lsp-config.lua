function onattach(client) 
	require('completion').on_attach(client)
end

require('lspfuzzy').setup({})
require('nvim-lightbulb').update_lightbulb()
require('compe').setup {
  enabled = true;
  autocomplete = true;
  debug = false;
  min_length = 1;
  preselect = 'enable';
  source_timeout = 200;
  incomplete_delay = 400;
  documentation = true;

  source = {
    path = true;
    buffer = true;
    nvim_lsp = true;
    nvim_lua = true;
    vsnip = true;
  };
}

local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.completion.completionItem.snippetSupport = true
capabilities.textDocument.completion.completionItem.resolveSupport = {
  properties = {
    'documentation',
    'detail',
    'additionalTextEdits',
  }
}

local lspconfig = require('lspconfig')
lspconfig.rust_analyzer.setup {
	on_attach = ra_onattach,
	capabilities = capabilities,
	settings = {
        	["rust-analyzer"] = {
			assist = {
                		importMergeBehavior = "last",
                		importPrefix = "by_self",
            		},
           		cargo = {
				loadOutDirsFromCheck = true
          		},
        		procMacro = {
     	 			enable = true
        		},
        	}
	}
}

lspconfig.jsonls.setup {
	on_attach = onattach,
	commands = {
		Format = {
        		function()
	        		vim.lsp.buf.range_formatting({},{0,0},{vim.fn.line("$"),0})
        		end
      		}
    	}
}

lspconfig.tsserver.setup {
	on_attach = onattach
}

lspconfig.html.setup {
	on_attach = ra_onattach,
	capabilities = capabilities
}

require('lspsaga').init_lsp_saga()
